import org.openkinect.freenect.*; //<>//
import org.openkinect.freenect2.*;
import org.openkinect.processing.*;

Kinect2 kinect;
int[] depth;

void setup() {
  size(1024, 848, P2D);
  kinect = new Kinect2(this);
  kinect.initVideo();
  kinect.initDepth();
  kinect.initIR();
  kinect.initRegistered();
  kinect.initDevice();
}

//void videoEvent(Kinect2 k) {
//  // There has been a video event!
//}

//void depthEvent(Kinect2 k) {
//  // There has been a depth event!
//}

void draw() {
  background(0);

  //-- Use Kinect as regular old webcam
  PImage cam = kinect.getVideoImage();
  image(cam, 0, 0, kinect.colorWidth*0.27, kinect.colorHeight*0.27);
  text("RGB", 0, 0, kinect.colorWidth, kinect.colorHeight);

  //-- Use Kinect as infrared cam
  PImage infraRedCam = kinect.getIrImage();
  image(infraRedCam, 0, kinect.depthHeight);
  text("Infrared", 10, kinect.depthHeight);

  //-- Use Kinect getting the color depth image
  PImage depthCam = kinect.getDepthImage();
  image(depthCam, kinect.depthWidth, 0);
  text("Depth", kinect.depthWidth+10, 10);

  //image which aligns all the depth values with the RGB camera ones. This can be accessed as follows:
  PImage alignedCam = kinect.getRegisteredImage();
  image(alignedCam, kinect.depthWidth, kinect.depthHeight);
  text("AlignedCam", kinect.depthWidth, kinect.depthHeight);
  
  //depth test
  depth = kinect.getRawDepth();
  text("depth: " + depth, 10, 315);

  fill(255);
  text("Framerate: " + (int)(frameRate), 10, 355);
}
